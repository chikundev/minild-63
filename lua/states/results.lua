-- chikun :: 2014-2015
-- Results state


-- Temporary state, removed at end of script
local ResultsState = PrototypeState()


-- On state create
function ResultsState:create(message)

	self.message = message or ""

	self.buttons = {
		{
			text = "Restart",
			x = 40, y = 240,
			w = 200, h = 80,
			code = function()
				cs:change("play")
			end
		},
		{
			text = "Credits",
			x = 400, y = 240,
			w = 200, h = 80,
			code = function()
				cs:load("credits")
			end
		}
	}
end


-- On state update
function ResultsState:update(dt) end


function ResultsState:onClick(x, y)

	for key, button in ipairs(self.buttons) do

		if (x >= button.x and y >= button.y and
		    x < button.x + button.w and
		    y < button.y + button.h) then

			button.code()
		end
	end
end


-- On state draw
function ResultsState:draw()

	lg.setColor(255, 255, 255)
	lg.draw(gfx.convo.bg.cafe, 0, 0)

	lg.setColor(0, 0, 0, 128)
	lg.rectangle('fill', 0, 0, 640, 360)

	lg.setColor(255, 255, 255)
		lg.setFont(cf.convo.name)
	lg.print(self.message, 16, 16)

	for key, button in ipairs(self.buttons) do

		lg.setColor(255, 255, 255)
		lg.rectangle('fill', button.x, button.y, button.w, button.h)

		lg.setColor(0, 0, 0)
		lg.printf(button.text, button.x, button.y + 25, button.w, 'center')
	end
end


-- On state kill
function ResultsState:kill()

	self.message = nil
end


function goToResults(message)

	cs:change("results", message)
end


-- Transfer data to state loading script
return ResultsState
