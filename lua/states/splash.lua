-- chikun :: 2014-2015
-- Splash state


-- Temporary state, removed at end of script
local SplashState = PrototypeState()


-- On state create
function SplashState:create()

	self.has_beeped = false
	self.timer = 0

	lg.setBackgroundColor(255, 255, 255)
end


-- On state update
function SplashState:update(dt)

	self.timer = self.timer + dt

	if (self.timer >= 0.5 and not self.has_beeped) then

		sfx.startup:play()

		self.has_beeped = true
	end

	if (self.timer > 1) then

		cs:change(DEFAULT_STATE or "play")
	end
end


-- On state draw
function SplashState:draw()

	local alpha = 1 - math.abs(self.timer - 0.5) * 2

	lg.setColor(0, 0, 0, alpha * 255)
	lg.setFont(cf.splash)

	lg.printf("chikun", GAME_WIDTH / 2,
	          (GAME_HEIGHT - cf.splash:getHeight()) / 2, 0, 'center')
end


-- On state kill
function SplashState:kill() end


-- Transfer data to state loading script
return SplashState
