-- chikun :: 2014-2015
-- Mobile state


-- Temporary state, removed at end of script
local MobileState = PrototypeState()

convs = require("lua/mobile_convos")


-- On state create
function MobileState:create(width, height)

	self.w, self.h = width, height

	self.tone = 2

	self.tones = {
		sfx.tone_airhorn,
		sfx.tone_bell,
		sfx.tone_chimes
	}

	self.states = {
		home = {
			bg = gfx.mobile.bg_home,
			buttons = {
				{
					x = 0,     y = 40,
					w = width, h = 40,
					img = gfx.mobile.button_mail,
					code = function(self)
						self.state = "mail"
					end
				},
				{
					x = 0,     y = 95,
					w = width, h = 40,
					img = gfx.mobile.button_snake,
					code = function(self)
						self.state = "snake"
					end
				},
				{
					x = 0,     y = 150,
					w = width, h = 40,
					img = gfx.mobile.button_settings,
					code = function(self)
						self.state = "settings"
					end
				},
				{
					x = 3,     y = 216,
					w = 15,    h = 18,
					img = gfx.mobile.nav_back
				},
				{
					x = 70,    y = 216,
					w = 20,    h = 20,
					img = gfx.mobile.nav_home
				}
			}
		},
		mail = {
			bg = gfx.mobile.bg_mail,
			buttons = {
				{
					x = 0,     y = 40,
					w = width, h = 40,
					img = gfx.mobile.button_contact_1,
					code = function(self)
						self.state = "convo"
						self.contact = 1
					end
				},
				{
					x = 0,     y = 95,
					w = width, h = 40,
					img = gfx.mobile.button_contact_2,
					code = function(self)
						self.state = "convo"
						self.contact = 2
					end
				},
				{
					x = 0,     y = 150,
					w = width, h = 40,
					img = gfx.mobile.button_contact_3,
					code = function(self)
						self.state = "convo"
						self.contact = 3
					end
				},
				{
					x = 12,     y = 40,
					w = 40, h = 40,
					img = gfx.mobile.contact_1,
					code = function(self)
						self.state = "convo"
						self.contact = 1
					end
				},
				{
					x = 12,     y = 95,
					w = 40, h = 40,
					img = gfx.mobile.contact_2,
					code = function(self)
						self.state = "convo"
						self.contact = 2
					end
				},
				{
					x = 12,     y = 150,
					w = 40, h = 40,
					img = gfx.mobile.contact_3,
					code = function(self)
						self.state = "convo"
						self.contact = 3
					end
				},
				{
					x = 3,     y = 216,
					w = 15,    h = 18,
					img = gfx.mobile.nav_back,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 70,    y = 216,
					w = 20,    h = 20,
					img = gfx.mobile.nav_home,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 126,   y = 220,
					w = 28,    h = 12,
					img = gfx.mobile.nav_send
				}
			}
		},
		convo = {
			bg = gfx.mobile.bg_mail,
			buttons = {
				{
					x = 3,     y = 216,
					w = 15,    h = 18,
					img = gfx.mobile.nav_back,
					code = function(self)
						self.state = "mail"
					end
				},
				{
					x = 70,    y = 216,
					w = 20,    h = 20,
					img = gfx.mobile.nav_home,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 126,   y = 220,
					w = 28,    h = 12,
					img = gfx.mobile.nav_send,
					send = true,
					code = function(self)

						local conv = self.convos[self.contact]

						local next_msg = math.min(conv.available + 1,
						                          #conv.texts)

						while (conv.texts[next_msg][2]) do

							conv.available = next_msg
							conv.seen = next_msg

							next_msg = math.min(conv.available + 1,
						                          #conv.texts)
						end

						if (conv.available < #conv.texts) then

							conv.timer = 9 + math.random(3)
						end
					end
				}
			}
		},
		settings = {
			bg = gfx.mobile.bg_settings,
			buttons = {
				{
					x = 3,     y = 216,
					w = 15,    h = 18,
					img = gfx.mobile.nav_back,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 70,    y = 216,
					w = 20,    h = 20,
					img = gfx.mobile.nav_home,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 0,     y = 40,
					w = width, h = 40,
					img = gfx.mobile.button_airhorn,
					code = function(self)
						cx.play(self.tones[1])
						heart:modify(-0.08)
						self.tone = 1
					end
				},
				{
					x = 0,     y = 95,
					w = width, h = 40,
					img = gfx.mobile.button_bell,
					code = function(self)
						cx.play(self.tones[2])
						heart:modify(-0.08)
						self.tone = 2
					end
				},
				{
					x = 0,     y = 150,
					w = width, h = 40,
					img = gfx.mobile.button_chimes,
					code = function(self)
						cx.play(self.tones[3])
						heart:modify(-0.08)
						self.tone = 3
					end
				}
			}
		},
		snake = {
			bg = gfx.mobile.bg_snake,
			buttons = {
				{
					x = 3,     y = 216,
					w = 15,    h = 18,
					img = gfx.mobile.nav_back,
					code = function(self)
						self.state = "home"
					end
				},
				{
					x = 70,    y = 216,
					w = 20,    h = 20,
					img = gfx.mobile.nav_home,
					code = function(self)
						self.state = "home"
					end
				}
			}
		}
	}

	self.convos = deepCopy(convs)

	self.state = "home"
end


function MobileState:click(x, y)

	for key, button in ipairs(self.states[self.state].buttons) do

		if (button.code and
		    x >= button.x and
		    y >= button.y and
		    x < button.x + button.w and
		    y < button.y + button.h) then

			button.code(self)

			if (self.state == "convo") then

				local conv = self.convos[self.contact]

				conv.seen = conv.available
			end
		end
	end
end


-- On state update
function MobileState:update(dt)

	for key, convo in ipairs(self.convos) do

		if (convo.timer > 0) then

			convo.timer = math.max(0, convo.timer - dt)

			while (convo.timer == 0 and convo.available < #convo.texts) do

				if (convo.texts[convo.available + 1][2]) then

					break
				end

				convo.available = convo.available + 1
			end
		end
	end

	if (self.state == "convo") then

		local conv = self.convos[self.contact]

		conv.seen = conv.available
	end
end


-- On state draw
function MobileState:draw()

	lg.setColor(255, 255, 255)

	lg.draw(self.states[self.state].bg, 0, 0)

	lg.draw(gfx.mobile.bar_top, 0, 0)
	lg.draw(gfx.mobile.icon_batt, 11, 5)
	lg.draw(gfx.mobile.bar_bottom, 0, 211)
	lg.setFont(cf.mobile.time)
	lg.printf(timer:getTime(), 100, 4, 53, 'right')

	for key, button in ipairs(self.states[self.state].buttons) do

		lg.setColor(255, 255, 255)
		if (not button.code) then

			lg.setColor(128, 128, 128)
		end
		if (self.state == "convo" and button.send) then

			local conv = self.convos[self.contact]

			local msg = math.min(#conv.texts, conv.available + 1)

			if (not conv.texts[msg][2]) then

				lg.setColor(128, 128, 128)
			end
		end
		lg.draw(button.img, button.x, button.y)
	end

	if (self.state == "mail") then

		for key, value in ipairs(self.convos) do

			local diff = value.available - value.seen

			if (diff > 0) then

				lg.setColor(200, 0, 0)
				lg.rectangle('fill', 36, 55 * key - 21, 12, 12)

				lg.setColor(255, 255, 255)
				lg.printf(diff, 42, 55 * key - 21, 0, 'center')
			end

			local last, text = value.texts[value.available], ""

			if (last[2]) then

				text = "> "
			end

			text = text .. last[1]

			if text:len() > 20 then

				text = text:sub(1, 17) .. "..."
			end

			lg.setColor(255, 255, 255)
			lg.print(text, 55, 55 * key + 12)
		end
	elseif (self.state == "convo") then

		local fnt, conv, y =
			lg.getFont(),
			self.convos[self.contact],
			210

		for i = conv.available, 1, -1 do

			local msg = conv.texts[i]
			local width, wrap = fnt:getWrap(msg[1], 96)
			wrap = wrap * fnt:getHeight()

			local x, text_offset = 8, 8

			if (not msg[2]) then

				width = width + 28
				x = 160 - width - 8
				text_offset = 28
				wrap = math.max(20, wrap)
			end

			y = y - wrap - 24

			lg.setColor(0, 0, 0, 128)
			lg.rectangle('fill', x, y, width + 16, wrap + 16)

			lg.setColor(255, 255, 255)
			lg.printf(msg[1], x + text_offset, y + 8, 96, 'left')

			if (not msg[2]) then

				lg.draw(gfx.mobile["contact_" .. self.contact], x + 4, y + 4, 0, 0.5)
			end
		end

	elseif (self.state == "home") then

		local messages = 0

		for key, value in ipairs(self.convos) do

			messages = messages + value.available - value.seen
		end

		if (messages > 0) then

			lg.setColor(200, 0, 0)
			lg.rectangle('fill', 36, 34, 12, 12)

			lg.setColor(255, 255, 255)
			lg.printf(messages, 42, 34, 0, 'center')
		end
	end

	lg.setColor(255, 255, 255)
end


-- On state kill
function MobileState:kill()

	self.x = nil
end


-- Transfer data to state loading script
return MobileState
