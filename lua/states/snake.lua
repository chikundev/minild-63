-- chikun :: 2014-2015
-- Snake state


-- Temporary state, removed at end of script
local SnakeState = PrototypeState()


-- On state create
function SnakeState:create()

	self.snake = {
		{ x = 88, y = 48},
		{ x = 80, y = 48},
		{ x = 72, y = 48},
		{ x = 64, y = 48}
	}

	self.timer = 1

	self.dir = "r"

	self.size = 8
end


-- On state update
function SnakeState:update(dt)

	if (self.timer <= 0) then

		self.timer = self.timer % 1

		local x, y = 0, 0

		for key, value in ipairs(self.snake) do

			local vx, vy = value.x, value.y

			if (key == 1) then

				if (self.dir == "r") then

					value.x = value.x + self.size
				elseif (self.dir == "l") then

					value.x = value.x - self.size
				elseif (self.dir == "u") then

					value.y = value.y - self.size
				else

					value.y = value.y + self.size
				end
			else

				value.x = x
				value.y = y
			end

			x, y = vx, vy
		end
	end
end


-- On state draw
function SnakeState:draw()

	lg.setColor(255, 255, 255)

	lg.print(lt.getFPS(), 480, 32)

	cg.drawCentred(gfx.sexy_halloween, 256, 256, self.rotation, 0.8)

	lg.translate(768, 128)

		cm.draw()
	lg.origin()
end


-- On state kill
function SnakeState:kill()

	self.x = nil
end


-- Transfer data to state loading script
return SnakeState
