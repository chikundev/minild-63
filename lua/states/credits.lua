-- chikun :: 2014-2015
-- Credits state


-- Temporary state, removed at end of script
local CreditsState = PrototypeState()


-- On state create
function CreditsState:create()

	cb.credits:play()
end


-- On state update
function CreditsState:update(dt) end

function CreditsState:onClick(x, y)

	cs:set("results")
end


-- On state draw
function CreditsState:draw()

	lg.setColor(255, 255, 255)
	lg.draw(gfx.credits, 0, 0)
end


-- On state kill
function CreditsState:kill()

	cb.credits:stop()
end


-- Transfer data to state loading script
return CreditsState
