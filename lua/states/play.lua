-- chikun :: 2014-2015
-- Play state


-- Temporary state, removed at end of script
local PlayState = PrototypeState()


-- On state create
function PlayState:create()

	-- Set background colour to black
	lg.setBackgroundColor(0, 0, 0)

	cc:reset()

	-- Set the starting convo
	cc:setConvo(START_CONVO, START_CONVO_STEP or 1)

	-- Create new mobile object at certain point
	self:createMobile(240, 424, 160, 240)

	-- Stores the last mouse position
	self.last_click = getClick()

	cb.normal:play()
end


-- On state update
function PlayState:update(dt)

	-- Find current mouse position
	local click = getClick()

	self.mobile:update(dt)
	timer:update(dt)

	-- Update conversation controller
	cc:update(dt)

	-- Update previous click
	self.last_click = click
end


function PlayState:onClick(x, y)

	local click = getClick()

	if (self.mobile:clicked(click.x, click.y)) then

		if (not self.mobile.open) then

			self.mobile.open = true
		elseif (self.mobile.y == self.mobile.y_open) then

			self.mobile.state:click(click.x - self.mobile.x,
			                        click.y - self.mobile.y)
		end
	elseif (self.mobile.open) then

		self.mobile.open = false
	else

		--if then end
		cc:progress(x, y)
	end
end


-- On state draw
function PlayState:draw()

	lg.setColor(255, 255, 255)

	-- Draw conversation background and text box
	lg.draw(gfx.convo.bg.cafe, 0, 0)
	lg.draw(gfx.convo.bar, 16, 264)

	-- If text isn't scrolling, speaker's mouth isn't animating
	local frame = nil
	if (not cc:isTextScrolling()) then frame = 1 end

	-- Draw speaker
	cc:getSpeaker():draw(320 - 128, 264 - 256, 4, frame)

	-- Draws name of speaker and their text
	lg.setFont(cf.convo.name)
	lg.print(cc:getSpeakerName(), 24, 256 - lg.getFont():getHeight())
	lg.setFont(cf.convo.text)
	lg.printf(cc:getText(), 24, 272, 592, 'left')

	-- Determine current dialogue choices
	local choices = cc:getChoices()

	-- Draw choices if they exist
	if (#choices > 0) then

		for key, choice in ipairs(choices) do

			local x, y, w, h = cc:getChoiceBox(key)

			lg.setColor(0, 0, 0, 192)
			lg.rectangle('fill', x, y, w, h)

			lg.setColor(255, 255, 255)
			lg.printf(choice.text, x, y + 6, w, 'center')
		end
	end

	-- If mobile is active, draw it and a cover
	if (self.mobile.active) then

		-- Draw full screen fade if mobile is pulled up
		local alpha = math.min((self.mobile.y - 32) / 288 * 128, 128)
		lg.setColor(0, 0, 0, 128 - alpha)
		lg.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

		-- Determine current canvas
		local tmp_canvas = lg.getCanvas()

		-- Clear and draw mobile to mobile canvas
		self.mobile.canvas:clear()
		lg.setCanvas(self.mobile.canvas)
			self.mobile.state:draw()
		lg.setCanvas(tmp_canvas)

		-- Draw mobile canvas at relevant position
		lg.draw(self.mobile.canvas, self.mobile.x, self.mobile.y)
		lg.draw(gfx.mobile.phone, self.mobile.x - 12, self.mobile.y - 48)
	end

	-- Draw heart
	local alpha, height =
		math.min(heart.interest * 2, 1) * 255,
		48 * heart.interest
	lg.setScissor(24, 72 - height, 53, height)
		lg.setColor(alpha, alpha, alpha)
		lg.draw(gfx.heart.full, 24, 24)
	lg.setScissor()
	lg.setColor(255, 255, 255)
	lg.draw(gfx.heart.outline, 24, 24)
end

-- On state kill
function PlayState:kill() end


function PlayState:createMobile(x, y, w, h)

	 -- Create the mobile
	self.mobile = {
		x = x, y = y, w = w, h = h,
		y_inactive = GAME_HEIGHT + 64,
		y_active   = GAME_HEIGHT + 24,
		y_open     = 64,
		active  = true,
		open    = false,
		r_speed = 256,     -- Speed of mobile appearing at bottom
		y_speed = 3200,    -- Speed of mobile moving up when clicked
		state   = cs.states["mobile"],
		canvas  = lg.newCanvas(w, h)
	}

	-- Initialise the mobile state
	self.mobile.state:create(w, h)

	function self.mobile:update(dt)

		if (self.open) then

			self.y = math.max(self.y_open, self.y - self.y_speed * dt)
		elseif (not self.active) then

			self.y = math.min(self.y_inactive, self.y + self.r_speed * dt)
		else

			if (self.y > self.y_active) then

				self.y = math.max(self.y_active, self.y - self.r_speed * dt)
			else

				self.y = math.min(self.y_active, self.y + self.y_speed * dt)
			end
		end

		self.state:update(dt)
	end


	function self.mobile:clicked(x, y, screen_only)

		if (screen_only) then

			return x > self.x and y > self.y and
		           x < self.x + self.w and
		           y < self.y + self.h
		else

			return x > self.x - 12 and y > self.y - 48 and
		           x < self.x + self.w + 12 and
		           y < self.y + self.h + 48
		end
	end
end


-- Transfer data to state loading script
return PlayState
