local bodies, heads =
	gfx.chars.croc.body,
	gfx.chars.croc.head

local mouths = {
	normal = {
		gfx.chars.croc.mouth.normal_shut,
		gfx.chars.croc.mouth.normal_open,
	},
	smoke = {
		gfx.chars.croc.mouth.smoke_shut,
		gfx.chars.croc.mouth.smoke_open
	}
}

local overs = {
	smoke = {
		anim_speed = 2,
		gfx.chars.croc.over.smoke_1,
		gfx.chars.croc.over.smoke_2
	}
}

local Croc = PrototypeSpeaker("Charles", bodies, heads, mouths, overs)

return Croc
