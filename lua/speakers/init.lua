-- State class on which all speakers are based
PrototypeSpeaker = class(function(new_class, name, bodies, heads, mouths, overs)

		new_class.name   = name or ""

		new_class.bodies = bodies or { }
		new_class.heads  = heads  or { }
		new_class.mouths = mouths or { }
		new_class.overs  = overs  or { }

		new_class.body  = ""
		new_class.head  = ""
		new_class.mouth = ""
		new_class.over  = ""

		new_class.mouth_step = 0
		new_class.over_step  = 0
	end)

function PrototypeSpeaker:getName(name)

	return self.name
end

function PrototypeSpeaker:setName(name)

	self.name = name
end

function PrototypeSpeaker:setStance(body, head, mouth, over)

	self.body  = body  or self.body
	self.head  = head  or self.head
	self.mouth = mouth or self.mouth
	self.over  = over  or self.over

	if (mouth) then

		self.mouth_step = 0
	end

	if (over) then

		self.over_step = 0
	end
end

function PrototypeSpeaker:update(dt, name)

	if (self.mouth ~= "" and not name) then

		local frames, speed =
			#self.mouths[self.mouth],
			self.mouths[self.mouth].anim_speed or 20

		self.mouth_step = (self.mouth_step + dt * speed) % frames
	else

		self.mouth_step = 0
	end

	if (self.over ~= "") then

		local frames, speed =
			#self.overs[self.over],
			self.overs[self.over].anim_speed or 2

		self.over_step = (self.over_step + dt * speed) % frames
	end
end

function PrototypeSpeaker:draw(x, y, scale, step)

	local mouth_step, over_step =
		step or math.floor(self.mouth_step) + 1,
		math.floor(self.over_step) + 1

	if (self.body and self.body ~= "") then

		love.graphics.draw(self.bodies[self.body], x, y, 0, scale)
	end
	if (self.head and self.head ~= "") then

		love.graphics.draw(self.heads[self.head], x, y, 0, scale)
	end
	if (self.mouth and self.mouth ~= "") then

		love.graphics.draw(self.mouths[self.mouth][mouth_step], x, y, 0, scale)
	end
	if (self.over and self.over ~= "") then

		love.graphics.draw(self.overs[self.over][over_step], x, y, 0, scale)
	end
end


local current_dir = ...
local speakers = { }

-- Get a table of files / subdirs from dir
local items = love.filesystem.getDirectoryItems(...)

-- Iterate through all files and load them into states
for key, val in ipairs(items) do

	-- Load file as long as it isn't init.lua
	if (val ~= "init.lua") then

		local name = val:sub(1, -5)
		speakers[name] = require(current_dir .. "/" .. name)
	end
end


return speakers
