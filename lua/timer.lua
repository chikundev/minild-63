
timer = {
	value  = 0
}

function timer:update(dt)

	self.value = self.value + dt * 6
end

function timer:getHour()

	return (math.floor(self.value / 3600) - 1) % 12 + 1
end

function timer:getMinutes()

	return (math.floor(self.value / 60) % 60)
end

function timer:getSeconds()

	return (self.value % 60)
end

function timer:getTime()

	return string.format("%02d:%02d", self:getHour(), self:getMinutes())
end
