return {
	{
		speaker = "none",
		text    = "Some feeling of guilt or resentment fills you as Charles leaves. "
	},
	{
		speaker = "none",
		text    = "Things weren't exactly going smoothly, but you feel the encounter wasn't exactly a good reflection of you."
	},
	{
		speaker = "none",
		text    = "As a date, friend, or a nobody, someone else has left your life and now you somehow have to cope with that fact and move on."
	},
	{
		speaker = "none",
		text    = "If only it were easier."
	},
}
