return {
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(Charles sighs.)"
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "You know, I get the impression you're really indifferent about the whole thing."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "You probably don't want to offend me, I'm guessing, and that's fine, but, yeah..."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "I was hoping to be a little more comfortable about being open like this."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Anyway, this is awkward. We should probably talk about something else..."
	}
}
