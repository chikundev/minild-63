return {
	{
		speaker = "croc",
		body    = "hands_up",
		head    = "happy",
		text    = "Great! I'm sorry I wasn't exactly clear, but I think " ..
		          "you'll understand. It's hard to make a first impression " ..
		          "when you're... well..."
	},
	{
		speaker = "croc",
		body    = "hands_up",
		head    = "looking_down",
		text    = "I mean, this isn't exactly the first date where this has " ..
		          "been an issue...",
		choices = {
			{
				text   = "Tell me about it.",
				future = { "step_6", nil }
			},
			{
				text = "Move on.",
				future = { "step_7", nil }
			}
		}
	}
}
