return {
    {
		speaker = "croc",
		body    = "tail",
		text    = "OK, so what do you think?"
	},
		choices = {
			{
				text   = "I think that's a good point.",
				future = { "step_8", nil }
			},
			{
				text   = "Yes, I agree.",
				future = { "step_8", nil }
			},
			{
				text   = "No.",
				future = { "step_9", nil }
			},
			{
				text   = "Maybe?",
				future = { "step_12", nil }
			},
		}
}
