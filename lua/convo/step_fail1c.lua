return {
	{
		speaker = "none",
		text    = "You figure that there's no point dwelling over how the date went."
	},
	{
		speaker = "none",
		text    = "Perhaps that's for the best."
	},
	{
		speaker = "none",
		text    = "You don't bother sending him an apology or even a message as a follow-up - it's too late for that anyway. "
	},
	{
		speaker = "none",
		text    = "The date was subpar, but it's time to move on. You don't have the time to care."
	}
}

