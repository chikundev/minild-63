return {
	{
		speaker = "croc",
		body    = "tail",
		text    = "..."
	},
	{
		speaker = "croc",
        body    = "tail",
		future = { "step_10a", nil },
		text    = "Are you paying attention to me?",
		choices = {
			{
				text   = "What? Oh, yeah, yeah.",
				future = { "step_11", nil }
			}
		}
	}
}
