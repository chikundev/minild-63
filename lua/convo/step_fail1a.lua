return {
	{
		speaker = "none",
		text    = "Yeah. That was a disaster."
	},
	{
		speaker = "none",
		text    = "It was an awkward, uncomfortable date from the start. "
	},
	{
		speaker = "none",
		text    = "You're a little glad it ended when it did."
	},
	{
		speaker = "none",
		text    = "Perhaps you hold quite a considerable amount of pity for him, but that's irrelevant now."
	},
	{
		speaker = "none",
		text    = "You doubt you'll hear from Charles again; probably in turn joining the list of potential partners he thinks he's been stung by."
	},
	{
		speaker = "none",
		text    = "So much for that."
	}
}

