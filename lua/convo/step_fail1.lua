

return {
	{
		speaker = "croc",
		name    = "",
		auto    = false,
		text    = "An abrupt silence penetrates the conversation."
	},
	{
		speaker = "croc",
		body    = "tail",
		head    = "frown",
		mouth   = "normal",
		text    = "Hey, listen..."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "This isn't working out. "
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Every time I mention something, we seem to clash."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "And your damn phone! Every time I've said something you've been on it, completely paying no attention to what I have to say at all."
	},
	{
		speaker = "croc",
		name = "You",
		text    = "Well, this isn't exactly your first failed date either."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Damn it! Cut it out."
	},
	{
		speaker = "croc",
		name    = "",
		text    = "Charles gets up from his seat."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "What I'm saying is that this has been painful."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "I thought we could talk as friends regardless of how things went in the future."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "But I'm just not feeling it. Not at all."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "So, I'm off. Goodbye."
	},
	{
		speaker = "none",
		text    = "You sit motionless as Charles storms out the door."
	},
	{
		speaker = "none",
		text    = "Well, that's it - there he goes. Another person out of your life."
	},
	{
		speaker = "none",
		text    = "How do you feel about that? ",
		choices = {
			{
				text   = "I'm glad it's over.",
				future = { "step_fail1a", nil }
			},
			{
				text   = "I'm glad he's left.",
				future = { "step_fail1b", nil }
			},
			{
				text   = "I really couldn't care less.",
				future = { "step_fail1c", nil }
			},
			{
				text = "You know, I'm hurt.",
                future = { "step_fail1d", nil }
			},
			{
				text = "I just want to win the game.",
                future = { "step_fail1e", nil }
			}
		}
	}
}
