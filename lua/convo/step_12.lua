return {
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(Charles sighs.)"
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "You know, I get the impression you're really indifferent about the whole thing."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "And that's fine, I guess I was just hoping that someone would care."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Or at least empathise."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Anyway, I guess we should probably move on..."
	}
}
