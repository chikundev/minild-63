return {
	{
		speaker = "croc",
		name    = "You",
		text    = "They do. I mean, you're a crocodile. I'm a human. I can't think of a more inappropriate sort of date and the idea itself                   makes me incredibly uncomfortable."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "What's more, you weren't honest in failing to bring this up, and that's a red flag if I've ever seen one. Just                             completetly unbelievable. "
	},
    {
		speaker = "croc",
        head    = "frown",
		text    = "Ugh. You people are all the same."
	},
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(You rise from your seat.)"
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "I'm disappointed that this has been a complete waste of time."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "So, I wish you the best of luck in finding someone, but I'm out. I am out of here."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "Have a nice day."
	},
	{
		speaker = "croc",
		code    = function()
			goToResults("Congratulations! You're kind-of a dickhead.")
		end,
		text    = "Yeah, thanks for nothing."
	}
}
