return {
	{
		speaker = "croc",
		name = "You",
		text    = "Yeah, I guess."
	},
	{
		speaker = "croc",
        body    = "tail",
		text    = "...But why is that?",
		choices = {
			{
				text   = "Be specific about the whole crocodile thing",
				future = { "step_23", nil }
			},
			{
				text = "Be obtuse about the whole crocodile thing",
				future = { "step_24", nil }
			},
            {
				text = "Uh, I don't know? Let's change the subject.",
				future = { "step_18", nil }
			},
            {
				text = "I forgot. Can you repeat the question?",
				future = { "step_19", nil }
			}
		}
	}
}
