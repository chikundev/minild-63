return {
	{
		speaker = "croc",
		name = "You",
		auto = true,
		text = "No, I can't blame you for that."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Thanks. Sometimes I think that what I'm doing can't be justified, but it's good to hear that someone agrees with what I'm                  doing."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Anyway, the point is that I find myself in this situation every time I enter the dating game, and it's completely                            frustrating, you know?"
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "I guess it serves to show how vapid and appearance-obsessed people can be, especially given that you can completely wipe                   away people from your existence with a click."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "And, I think it follows, that's how people feel about crocodiles in general."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "I mean, nobody talks to me, people avoid me when I'm walking about in the street, and that hurts. I understand that you                    don't see crocodiles every day..."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "I mean, nobody talks to me, people avoid me when I'm walking about in the street, and that hurts. I understand that you                    don't see crocodiles every day..."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "But I'd appreciate it if people could just see me without fearing I'm going to gore them in a single bite."
	},
	{
		speaker = "croc",
        body    = "tail",
		future	= { "step_9a", nil},
		text    = "I mean, what about you? Are you afraid of crocodiles...? ",
		choices = {
			{
				text   = "Yes.",
				future = { "step_13", nil }
			},
			{
				text = "No.",
				future = { "step_14", nil }
			}
		}
	}
}
