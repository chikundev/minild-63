return {
	{
		speaker = "croc",
		body    = "tail",
		text    = "..."
	},
	{
		speaker = "croc",
        body    = "tail",
		future = { "step_10b", nil },
		text    = "I get the impression you're not listening.",
		choices = {
			{
				text   = "Sorry... Yeah, I'm listening.",
				future = { "step_11", nil }
			}
		}
	}
}
