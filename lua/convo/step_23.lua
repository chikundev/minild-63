return {
	{
		speaker = "croc",
		name = "You",
		text    = "Well, uh, the fact is that most crocodiles are carnivorous."
	},
    {
		speaker = "croc",
		body    = "You",
		text    = "Saltwater crocodiles particularly are dangerous to humans in estuaries, and attacks frequently occur where the person is unaware of the danger as the crocodile is underwater."
	},
    {
		speaker = "croc",
		name = "You",
		text    = "Most people aren't being rude when they avoid crocodiles, they are in fact attempting to preserve the privacy and habitat of the animal as well as to take precautions against possible attacks."
	},
    {
		speaker = "croc",
		body    = "You",
		text    = "So, I mean, yes, it's fear, but it's not irrational and also it's out of respect for the animal as much as it is recognition that they are generally dangerous animals to be around."
	}
}
