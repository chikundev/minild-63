return {
	{
		speaker = "croc",
        body    = "tail",
		future	= { "step_16", nil},
		text    = "Uh, I asked you if you were afraid of crocodiles.",
		choices = {
			{
				text   = "Oh, oh yeah. Definitely.",
				future = { "step_13", nil }
			},
			{
				text = "No, of course not! Ha ha...hah...",
				future = { "step_14", nil }
			},
			{
				text = "No, not really.",
				future = { "step_14", nil }
			}
		}
	}
}
