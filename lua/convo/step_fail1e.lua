return {
	{
		speaker = "none",
		text    = "You're talking about the dating game and not about the game itself, right?"
	},
	{
		speaker = "none",
		text    = "Because that would dangerously border the realm of metacommentary."
	},
	{
		speaker = "none",
		code    = function()
			goToResults("You fuckin' failed")
		end,
		text    = "Anyway, you should probably try again."
	},
}
