return {
	{
		speaker = "croc",
		name    = "You",
		text    = "Yeah, this isn't encouraging. I get the impression that you're being dishonest and a little obsessive about the whole 						dating thing."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "How can you possibly hope for people to like you in the long-term if you continue to lie about who you are? It isn't 						ethical or practical at all."
	},
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(You rise from your seat.)"
	},
	{
		speaker = "croc",
		name    = "You",
		head    = "frown",
		text    = "So, yeah. I might give this date a miss."
	},
	{
		speaker = "croc",
        head    = "frown",
		text    = "You're still judging me about this?"
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "I'm disappointed that this has been a complete waste of time."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "I guess so. I'm sorry this has been a waste of time for you, but I'm out."
	},
    {
		speaker = "croc",
		name    = "You",
		text    = "Have a nice day."
	},
	{
		speaker = "croc",
		text    = "Yeah, thanks for nothing."
	}
}
