return {
	{
		speaker = "croc",
		body    = "tail",
		auto	= false,
		text    = "Wow, you 'can' blame me? You know, that's a little harsh."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "Maybe I overestimated how much you were willing to put the whole crocodile thing aside..."
	},
	{
		speaker = "croc",
        body    = "tail",
		text    = "I mean...yeah, is what I did really that problematic to you?",
		choices = {
			{
				text   = "Yes.",
				future = { "step_25", nil }
			},
			{
				text = "No.",
				future = { "step_12", nil }
			}
		}
	}
}



