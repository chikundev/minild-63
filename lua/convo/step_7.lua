return {
	{
		speaker = "croc",
		body    = "tail",
		head    = "normal",
		mouth   = "normal",
		text    = "I guess it's best not to dwell on that sort of thing anyway."
	},
    {
		speaker = "croc",
		name = "You",
		body    = "tail",
		text    = "Yeah. So, uh... Well, tell me about yourself."
	},
	{
		speaker = "croc",
		body    = "tail",
		auto	= true,
		text    = "Well, I'm currently twenty-four, I work in retail at an electronics store. I actually moved out of my parent's house just last month, so things have been a little different recently for me."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "So I rent an apartment by myself now and the disposable income I used to throw away on anything has taken a nosedive. I used to listen to a lot of music, collect movies... I don't think I'll be doing that for the time being."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Not unless I suddenly become a manager, but that's just a little fantasy of mine, I guess..."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Uh. So anyway, I've been in consumer electronics for a long time, but I was a little bit of an underachiever in school."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Mostly because there were some clear difficulties in getting to know the other students and teachers in school, making friends, you know..."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "I'd just hang out under a drainage ditch and smoke during break and not pay attention in class. But that's all behind me now. I'm trying to do a few things to better myself... Like being a little more confident about dating."
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Say, speaking of smoking, you wouldn't mind if I had a smoke? We're in a cafe: nobody's going to mind, after all."
	},
}
