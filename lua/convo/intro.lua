return {
	{
		speaker = "none",
		text    = "The State Cafe is a small establishment in the centre of " ..
		          "a nondescript urban\narcade."
	},
	{
		speaker = "none",
		text    = "Its atmosphere is relaxed with the sound of only a quiet " ..
		          "chatter."
	},
	{
		speaker = "none",
		text    = "The smell of espresso and pastries fills the air. There " ..
		          "are more unpleasant\nplaces to stage a date, you figure."
	},
	{
		speaker = "none",
		text    = "Midday. You are right on time."
	},
	{
		speaker = "none",
		text    = "You scan the cafe for the date in question and find him " ..
		          "sitting in a corner\ntable."
	},
	{
		speaker = "none",
		text    = "He is a crocodile."
	},
	{
		speaker = "none",
		text    = "You didn't expect your date to be a crocodile."
	},
	{
		speaker = "croc",
		body    = "tail",
		head    = "normal",
		mouth   = "normal",
		text    = "Hey, I'm Charles. Nice to meet you."
	},
	{
		speaker = "croc",
		name    = "",
		text    = "(You shake hands with the crocodile.)"
	},
	{
		speaker = "croc",
		name    = "",
		text    = "(The implications of your date being a crocodile make "..
		          "you a little uncomfortable.)"
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "Likewise..."
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "You, uh, didn't tell me that you were a crocodile when "..
		          "we talked online."
	},
	{
		speaker = "croc",
		head    = "looking_down",
		body    = "hand_up",
		text    = "Sorry... I guess I forgot that little detail."
	},
	{
		speaker = "croc",
		head    = "normal",
		body    = "tail",
		text    = "I hope that's not a problem?",
		choices = {
			{
				text   = "Well... Yes, it is a little problem.",
				future = { "step_2", nil }
			},
			{
				text = "No, I don't really mind.",
                future = { "step_5", nil }
			}
		}
	}
}
