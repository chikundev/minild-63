return {
	{
		speaker = "croc",
		name    = "You",
		text    = "Well, yes. It is a bit of a problem. I hate to say it, " ..
		          "but I mean, you're a crocodile. I am not. There are some " ..
		          "irreconcilable tensions that come up here."
	},
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(Charlie frowns.)"
	},
	{
		speaker = "croc",
		body    = "hands_hips",
		text    = "I think that's pretty presumptuous of you."
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "Well, it's not that simple-"
	},
	{
		speaker = "croc",
		body    = "hands_up",
		text    = "You haven't even talked to me for a minute, and you're " ..
		          "going to judge me based on my appearance? That's the " ..
		          "impression I'm getting."
	},
	{
		speaker = "croc",
		name    = "You",
		text    = "I just wish you could have at least told me about this " ..
		          "before we met."
	},
	{
		speaker = "croc",
		body    = "hand_up",
		head    = "sad",
		text    = "So what?"
	},
	{
		speaker = "croc",
		body    = "tail",
		text    = "Listen, however this works out, don't you at least think " ..
		          "it's worth getting to know each other? Do your " ..
		          "prejudices really mean that much to you?",
		choices = {
			{
				text   = "They do.",
				future = { "step_4", nil }
			},
			{
				text = "I guess they really don't...",
				future = { "step_5", nil }
			}
		}
	}
}
