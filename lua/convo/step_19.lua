return {
	{
		speaker = "croc",
        body    = "tail",
		text    = "I asked you why you're apparently so afraid of crocodiles.",
		choices = {
			{
				text   = "Be specific about the whole crocodile thing",
				future = { "step_23", nil }
			},
			{
				text = "Be obtuse about the whole crocodile thing",
				future = { "step_24", nil }
			},
            {
				text = "Yeah, I forget - let's change the subject.",
				future = { "step_18", nil }
			}
		}
	}
}
