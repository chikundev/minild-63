return {
	{
		speaker = "croc",
		auto	= false,
		head	= "frown",
		text    = "...Forget it. "
	},
    {
		speaker = "croc",
		head	= "frown",
		text    = "You're giving me the silent treatment, aren't you?"
	},
	{
		speaker = "croc",
		head	= "frown",
		text    = "I understand that my problems mustn't be that important to you..."
	},
	{
		speaker = "croc",
		head	= "frown",
		text    = "But this is disrespectful."
	},
	{
		speaker = "croc",
		head	= "frown",
		text    = "Yeah, I'm out of here."
	},
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(Charles rises from his seat and leaves.)"
	},
	{
		speaker = "croc",
		name    = "",
		head    = "frown",
		text    = "(Looks like the date is over for you both.)"
	},
	{
		speaker = "none",
		text    = "(How do you feel about that?)",
		choices = {
			{
				text   = "I'm glad it's over.",
				future = { "step_fail1a", nil }
			},
			{
				text   = "I'm glad he's left.",
				future = { "step_fail1b", nil }
			},
			{
				text   = "I really couldn't care less.",
				future = { "step_fail1c", nil }
			},
			{
				text = "You know, I'm hurt.",
                future = { "step_fail1d", nil }
			},
			{
				text = "I just want to win the game.",
                future = { "step_fail1e", nil }
			}
		}
	}
}
