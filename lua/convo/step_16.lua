return {
	{
		speaker = "croc",
		body	= "tail",
		text    = "Right..."
	},
	{
		speaker = "croc",
		body	= "tail",
		text    = "I get it, you don't want to say anything because you'll incriminate yourself."
	},
	{
		speaker = "croc",
		body	= "tail",
		text    = "I guess I shouldn't be surprised that you might actually dislike crocodiles as much as everyone else."
	},
	{
		speaker = "croc",
		body	= "tail",
		future	= { "step_7", nil},
		text    = "But anyway..."
	},
}
