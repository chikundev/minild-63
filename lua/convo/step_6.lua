return {
	{
		speaker = "croc",
		body    = "tail",
		text    = "Well, it goes something like this..."
	},
	{
		speaker = "croc",
		name    = "",
		text    = "(This date is already getting fairly dull.)"
	},
	{
		speaker = "croc",
		name    = "",
		text    = "(You decide to check your phone.)"
	},
    {
		speaker = "croc",
		body    = "tail",
		auto	= true,
		text    = "Every date I've ever been on has developed well up until the stage where I meet people in person, for, well, the obvious reason."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "As you can imagine, by the point in which people see me, moreover notice that I'm different..."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "...that's where nearly every single contact has completely cut themselves off."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "And perhaps it's fair to say that I'm not honest when I fail to tell them that I'm a crocodile..."
	},
    {
		speaker = "croc",
		body    = "tail",
		text    = "But invariably, in every single case, the relationship ends at the point I either tell them or they realise they're dating a crocodile. "
	},
	{
		speaker = "croc",
        body    = "tail",
		future  = { "step_10", nil},
		text    = "Can you blame me for just wanting people to warm to my personality and ignore the whole appearance thing, whether they  could know about it or not? ",
		choices = {
			{
				text   = "Yes.",
				future = { "step_8", nil }
			},
			{
				text = "No.",
				future = { "step_9", nil }
			}
		}
	}
}
