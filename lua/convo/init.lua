local ConversationController = { }

heart = {
	interest = 0.5
}

function ConversationController:reset()

	self.auto  = false
	self.choice_time = 10
	self.talk_time   = 0.05 -- This is multiplied by the number of chars in text
	self.timer = 0

	heart.interest = 0.5
end

ConversationController:reset()

function heart:modify(value)

	self.interest = math.clamp(0, self.interest + value, 1)

	if (self.interest == 0) then

		-- END GAME IN BAD NEGATIVE ENDING
		cc:setConvo("step_fail1", nil)
		cb.normal:stop()
	elseif (self.interest == 1) then

		-- END GAME IN BAD POSITIVE ENDING
		cc:setConvo("ending_bad_pos", nil)
		cb.normal:stop()
	end
end

function ConversationController:setConvo(convo, step)

	self.current    = self.convos[convo] or self.current
	self.convo_step = step or 1
	self.speaker    = speakers[self.current[self.convo_step].speaker]
	self.text_step  = 0

	local cv = self.current[self.convo_step]
	self.speaker:setStance(cv.body, cv.head, cv.mouth, cv.over)

	if (self.current[self.convo_step].auto ~= nil) then

		self.auto = self.current[self.convo_step].auto

		if (not self.auto) then

			cs.states["play"].mobile.open = false
		end
	end
end


function ConversationController:getChoiceBox(key)

	local x, y, w, h =
		160,
		256 / (#(self:getChoices()) + 1) * key - 16,
		320,
		32

	return x, y, w, h
end


function ConversationController:getChoices()

	return self.current[self.convo_step].choices or {}
end


function ConversationController:getSpeaker()

	return self.speaker
end


function ConversationController:getSpeakerName()

	if (self.speaker) then

		return self.current[self.convo_step].name or self.speaker:getName()
	else

		return ""
	end
end


function ConversationController:getText()

	local min_text_step = math.floor(self.text_step)

	if (min_text_step == 0) then

		return ""
	else

		return self.current[self.convo_step].text:sub(1, min_text_step)
	end
end


function ConversationController:isTextScrolling()

	return self.text_step ~= self.current[self.convo_step].text:len()
end


function ConversationController:progress(x, y, prog)

	-- Find current mouse position and length of text
	local click, max_text_step =
		getClick(),
		self.current[self.convo_step].text:len()

	-- Scroll to end of text if not all showing or in auto mode
	if (self.text_step < max_text_step and not self.auto) then

		self.text_step = max_text_step
	elseif ((self.current[self.convo_step].choices and not prog) or
	        (self.auto and not prog)) then

		for key, choice in ipairs(self:getChoices()) do

			local box_x, box_y, box_w, box_h = self:getChoiceBox(key)

			if (click.x > box_x and click.y > box_y and
				click.x < box_x + box_w and click.y < box_y + box_h) then

				if (choice.code) then

					choice.code()
				end

				self:setConvo(choice.future[1], choice.future[2])

				sfx.choice:play()
			end
		end
	else

		local code, future =
			self.current[self.convo_step].code,
			self.current[self.convo_step].future

		if (auto ~= nil) then

			self.auto  = auto
		end
		self.timer = 0

		-- Run code if convo step has code
		if (code) then

			code()
		end

		-- If next_convo then jump to that, or just progress
		if (future) then

			self:setConvo(future[1], future[2])
		elseif (self.convo_step < #self.current) then

			self:setConvo(nil, self.convo_step + 1)
		end

		sfx.click:play()
	end
end


function ConversationController:update(dt)

	-- Whether the mobile is active depends on the auto variable
	cs.states["play"].mobile.active = self.auto

	if (self.auto) then

		-- Increase or decrease heart depending on game mode
		local modifier = 0.008
		if (cs.states["play"].mobile.open) then

			modifier = -modifier * 2
		end
		heart:modify(modifier * dt)

		local time = self.talk_time * self.current[self.convo_step].text:len()
		if (self.current[self.convo_step].choices) then

			time = self.choice_time
		end

		time = self.current[self.convo_step].time or time

		self.timer = self.timer + dt

		if (self.timer >= time) then

			self:progress(0, 0, true)
		end
	end

	if (self.speaker) then

		self.speaker:update(dt, self.current[self.convo_step].name)
	end

	self.text_step = math.min(self.text_step + dt * 256,
	                          self.current[self.convo_step].text:len())
end


local function loadConvoFolder(folder, tab)

	-- Table is either new table or given
	local tab = tab or {}

	-- Obtain table of items in directory
	local items = love.filesystem.getDirectoryItems(folder)

	-- Iterate through all items in directory
	for key, item in ipairs(items) do

		-- Check if item is a file or folder and act accordingly
		if (lf.isFile(folder .. "/" .. item)) then

			-- Split item into name and type of file
			local name = item:sub(1, -5)

			-- If item is either jpg or png...
			if (name ~= "init") then

				-- ...load image into table
				tab[name] = require(folder .. "/" .. name)
			end
		else

			-- Create new table and load folder recursively
			tab[item] = {}
			loadConvoFolder(folder .. "/" .. item, tab[item])
		end
	end

	-- Return loaded table
	return tab
end


-- Load the convo folder into the convo table
ConversationController.convos = loadConvoFolder(...)

return ConversationController
