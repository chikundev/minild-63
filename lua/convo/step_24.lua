return {
	{
		speaker = "croc",
		name = "You",
		text    = "Well, you know, some crocodiles, uh...they can be dangerous."
	},
    {
		speaker = "croc",
		body    = "You",
		text    = "Not all crocodiles. But - um, you know, there are some bad sorts, and you can never tell."
	},
    {
		speaker = "croc",
		name = "You",
		text    = "You've got to be careful. "
	},
    {
		speaker = "croc",
		body    = "You",
		text    = "But I stress...not all crocodiles. Just some, right? That's the only reason."
	}
}
