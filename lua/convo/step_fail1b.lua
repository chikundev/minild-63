return {
	{
		speaker = "none",
		text    = "You lean back, and a tension leaves your body. That was uncomfortable."
	},
	{
		speaker = "none",
		text    = "You can't find a shred of sympathy for Charles, in a some part because he was difficult and incapable of relaxed conversation, but mostly because he was a crocodile."
	},
	{
		speaker = "none",
		text    = "Imagine that! Dating a crocodile!"
	},
	{
		speaker = "none",
		text    = "You chuckle to yourself as you realize you will have to foot his bill, and your smile abruptly turns to a scowl."
	},
	{
		speaker = "none",
		text    = "Yeah, this has certainly been a waste of time."
	},
}

