require(... .. "/lib")

-- Load resources
cb = require("/bgm")
cc = require(... .. "/convo")
cf = require("/fnt")
cg = require("/gfx")
ci = require(... .. "/input")
cs = require(... .. "/states")
cx = require("/sfx")

speakers = require(... .. "/speakers")
require(... .. "/timer")
