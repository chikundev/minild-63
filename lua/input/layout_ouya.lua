-- chikun :: 2015
-- Control scheme for OUYA controller

return {
	up    = { "joy_button_12", "joy_axis_2_neg" },
	left  = { "joy_button_14", "joy_axis_1_neg" },
	down  = { "joy_button_13", "joy_axis_2_pos" },
	right = { "joy_button_15", "joy_axis_1_pos" },
	a     = { "joy_button_1" },
	b     = { "joy_button_2" },
	start = { "key_menu" }
}
