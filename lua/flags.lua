-- chikun :: 2015
-- Game flags


DEFAULT_SCALE = 1
FULLSCREEN    = false
GAME_VSYNC    = true
GAME_WIDTH    = 640
GAME_HEIGHT   = 360

IS_OUYA        = false  -- Whether or not the game is running on an OUYA
IS_PIXEL_BASED = true   -- Whether or not the game is pixel based

START_CONVO      = "intro"  -- Conversation which game will start at
START_CONVO_STEP = 1        -- Conversation step game will start at

-- Load test flags is they exist on the system
if (love.filesystem.exists("lua/test_flags.lua")) then

	require("lua/test_flags")
end
