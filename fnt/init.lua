-- chikun :: 3015
-- Load all fonts


local fnt = {

	splash = love.graphics.newFont(... .. "/exo2.otf", 80),
	convo = {
		name = love.graphics.newFont(... .. "/targa.ttf", 20),
		text = love.graphics.newFont(... .. "/targa.ttf", 16)
	},
	mobile = {
		time = love.graphics.newFont(... .. "/sans.ttf", 9)
	}
}

return fnt
