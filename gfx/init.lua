-- chikun :: 2014-2015
-- Loads all graphics from the /gfx folder


cg = {}    -- Table holding all chikun graphical functions


--[[
	Draw an image centred on x and y
	INPUT:  x and y are the centred coordinates.
	        r is the rotation, sx and sy are the scale values.
]]
function cg.drawCentred(image, x, y, r, sx, sy)

	love.graphics.draw(image, x, y, r or 0, sx or 1, sx or sy or 1,
	        image:getWidth() / 2, image:getHeight() / 2)
end


--[[
	Recursively checks a folder for graphics and adds any found to a table
	INPUT:  x1 and y1 are the coordinates of the first point.
	        x2 and y2 are the coordinates of the second point.
	OUTPUT: A table
]]
local function loadGFXFolder(folder, tab)

	-- Table is either new table or given
	local tab = tab or {}

	-- Obtain table of items in directory
	local items = love.filesystem.getDirectoryItems(folder)

	-- Iterate through all items in directory
	for key, item in ipairs(items) do

		-- Check if item is a file or folder and act accordingly
		if (lf.isFile(folder .. "/" .. item)) then

			-- Split item into name and type of file
			local name, ext =
				item:sub(1, -5),
				item:sub(-3)

			-- If item is either jpg or png...
			if (ext == "jpg" or ext == "png") then

				-- ...load image into table
				tab[name] = love.graphics.newImage(folder .. "/" .. item)
			end
		else

			-- Create new table and load folder recursively
			tab[item] = {}
			loadGFXFolder(folder .. "/" .. item, tab[item])
		end
	end

	-- Return loaded table
	return tab
end


-- Load the gfx folder into the gfx table
gfx = loadGFXFolder(...)


return cg
